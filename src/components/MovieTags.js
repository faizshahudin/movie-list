import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Chip from "@material-ui/core/Chip";

const useStyles = makeStyles(theme => ({
  chip: {
    marginRight: theme.spacing(1),
    marginBottom: theme.spacing(1)
  }
}));

export default ({ genres }) => {
  const classes = useStyles();

  return (
    <Box>
      {genres.map(item => (
        <Chip label={item.label} className={classes.chip} />
      ))}
    </Box>
  );
};
