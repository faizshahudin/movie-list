import React from "react";
import { withRouter } from "react-router";
import EllipsisText from "react-ellipsis-text";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import useMediaQuery from "@material-ui/core/useMediaQuery";

const useStyles = makeStyles(theme => ({
  card: {
    [theme.breakpoints.down("sm")]: {
      width: "100%"
    },
    width: 200,
    height: 256,
    marginBottom: 10
  },
  title: {
    fontSize: "1rem",
    fontFamily: "Roboto",
    fontWeight: 400,
    lineHeight: 1.5,
    letterSpacing: "0.00938em"
  }
}));

export default withRouter(({ id, url, title, history }) => {
  const classes = useStyles();
  const mobile = useMediaQuery("(max-width:400px)");

  return (
    <Card className={classes.card}>
      <CardActionArea onClick={() => history.push(`/detail/${id}`)}>
        <CardMedia
          component="img"
          image={url}
          title={title}
          alt={title}
          height={200}
        />
        <CardContent>
          <EllipsisText
            text={title}
            className={classes.title}
            length={mobile ? 30 : 20}
          />
        </CardContent>
      </CardActionArea>
    </Card>
  );
});
