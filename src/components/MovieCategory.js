import React from "react";
import Slider from "react-slick";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import MovieCard from "./MovieCard";

const useStyles = makeStyles({
  container: {
    // border: "1px solid #000"
  },
  title: {
    marginTop: 10,
    marginBottom: 5
  }
});

const sliderSettings = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 5,
  slidesToScroll: 5,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4
      }
    },
    {
      breakpoint: 870,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 400,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
};

export default ({ title, list }) => {
  const classes = useStyles();

  return (
    <Container className={classes.container}>
      <Box className={classes.title}>
        <Typography variant="h6">{title}</Typography>
      </Box>
      <Slider {...sliderSettings}>
        {list.map(item => (
          <MovieCard id={item.id} url={item.images[0].url} title={item.title} />
        ))}
      </Slider>
    </Container>
  );
};
