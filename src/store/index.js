import React, { createContext, useContext, useReducer } from "react";

export const Store = createContext();

export const StoreProvider = ({ reducer, initialState, children }) => (
  <Store.Provider value={useReducer(reducer, initialState)}>
    {children}
  </Store.Provider>
);

export const useStateValue = () => useContext(Store);

export const initialState = {
  movie: {
    page: 1,
    reachedLastPage: false,
    movies: [],
    moviesFetch: false,
    moviesFetchError: "",
    movie: "",
    movieFetch: false,
    movieFetchError: ""
  }
};
