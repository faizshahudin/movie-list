import movieReducer from "./movie";

export const mainReducer = ({ movie }, action) => {
  const reducedStates = { movie: movieReducer(movie, action) };
  console.group(`${action.type}`);
  console.log("state-before-->", { movie });
  console.log("action-->", action);
  console.log("state-after-->", reducedStates);
  console.groupEnd();
  return reducedStates;
};
