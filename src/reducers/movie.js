export default (state, action) => {
  switch (action.type) {
    case 'page-assign': 
      return { ...state, page: action.payload }
    case "movies-fetch-pending":
      return { ...state, moviesFetch: true, moviesFetchError: "" };
    case "movies-fetch-fulfilled":
      return {
        ...state,
        movies: [...state.movies, ...action.payload],
        moviesFetch: false,
        reachedLastPage: action.payload.length ? false : true
      };
    case "movies-fetch-failed":
      return { ...state, moviesFetchError: action.payload, moviesFetch: false };
    case "movie-fetch-pending":
      return { ...state, movieFetch: true, movieFetchError: "" };
    case "movie-fetch-fulfilled":
      return {
        ...state,
        movie: action.payload,
        movieFetch: false
      };
    case "movie-fetch-failed":
      return { ...state, movieFetchError: action.payload, movieFetch: false };
    default:
      return state;
  }
};
