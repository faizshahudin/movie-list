import axios from "axios";

export const getMovies = async ({ page, dispatch }) => {
  try {
    dispatch({ type: "movies-fetch-pending" });

    const {
      data: { data }
    } = await axios.get(
      `https://cdn-discover.hooq.tv/v1.2/discover/feed?region=ID&page=${page}&perPage=20`
    );

    const movies = data.filter(
      item => item.type === "Multi-Title-Manual-Curation"
    );

    dispatch({ type: "movies-fetch-fulfilled", payload: movies });
  } catch (err) {
    dispatch({ type: "movies-fetch-failed", payload: err });
  }
};

export const getMovie = async ({ movieId, dispatch }) => {
  try {
    dispatch({ type: "movie-fetch-pending" });

    const {
      data: { data }
    } = await axios.get(
      `https://cdn-discover.hooq.tv/v1.2/discover/titles/${movieId}`
    );

    dispatch({ type: "movie-fetch-fulfilled", payload: data });
  } catch (err) {
    dispatch({ type: "movie-fetch-failed", payload: err });
  }
};
