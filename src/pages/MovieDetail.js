import React, { useEffect, Fragment } from "react";
import { withRouter } from "react-router";
import { getMovie } from "../actions/movie";
import { useStateValue } from "../store";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import MovieTags from "../components/MovieTags";
import { displayDuration,getCastsName } from "../utils";

const useStyles = makeStyles(theme => ({
  loaderWrapper: {
    textAlign: "center",
    marginTop: theme.spacing(2)
  },
  gridItem: {
    marginTop: theme.spacing(2)
  },
  title: {
    marginLeft: 0,
    marginBottom: theme.spacing(2)
  },
  row: {
    display: "flex",
    flexDirection: "row",
    marginBottom: theme.spacing(2)
  },
  subtext: {
    fontWeight: 300,
    marginRight: theme.spacing(2)
  },
  description: {
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(2)
  },
  label: {
    marginRight: theme.spacing(2),
    marginBottom: theme.spacing(2)
  }
}));

export default withRouter(({ match: { params: { movieId } } }) => {
  const classes = useStyles();
  const [{ movie }, dispatch] = useStateValue();

  const mobileView = useMediaQuery('(max-width: 600px)');

  const movieDetail = movie.movie;
  const movieCasts = movieDetail
    ? movieDetail.people.filter(item => item.role.toLowerCase() === "cast")
    : [];
  const directorName = movieDetail && movieDetail.people.length ? movieDetail.people[0].name : ''

  useEffect(() => {
    getMovie({ movieId, dispatch });
  }, [dispatch, movieId]);

  return (
    <Container maxWidth="lg">
      {movieDetail ? (
        <Grid container spacing={3}>
          <Grid item sm={4}>
            <Box className={classes.gridItem}>
              {mobileView ? (
                <Fragment>
<Typography variant={mobileView ? 'h5':'h2'} className={classes.title}>
                {movieDetail.title}
              </Typography>
              <Box className={classes.row}>
                <Typography variant={mobileView ? 'h6' : 'h5'} className={classes.subtext}>
                  {movieDetail.meta.releaseYear}
                </Typography>
                <Typography variant={mobileView ? 'h6' : 'h5'} className={classes.subtext}>
                  {displayDuration(movieDetail.running_time_friendly)}
                </Typography>
              </Box>
              <MovieTags genres={movieDetail.tags} />
                </Fragment>) : (
              <Card className={classes.card}>
                <CardMedia
                  component="img"
                  image={movieDetail.images[0].url}
                  title={movieDetail.title}
                  alt={movieDetail.title}
                />
              </Card>)}
            </Box>
          </Grid>
          <Grid item sm={8}>
            <Box className={classes.gridItem}>
              {!mobileView ? (
                <Fragment>
<Typography variant={mobileView ? 'h3':'h2'} className={classes.title}>
                {movieDetail.title}
              </Typography>
              <Box className={classes.row}>
                <Typography variant="h5" className={classes.subtext}>
                  {movieDetail.meta.releaseYear}
                </Typography>
                <Typography variant="h5" className={classes.subtext}>
                  {displayDuration(movieDetail.running_time_friendly)}
                </Typography>
              </Box>
              <MovieTags genres={movieDetail.tags} />
                </Fragment>
              ): (<Card className={classes.card}>
                <CardMedia
                  component="img"
                  image={movieDetail.images[0].url}
                  title={movieDetail.title}
                  alt={movieDetail.title}
                />
              </Card>)}
              <Typography className={classes.description}>{movieDetail.description}</Typography>
              {(movieCasts.length || directorName) && (
                <Grid container spacing={3}>
                  {movieCasts.length && (
                    <Fragment>
                      <Grid item xs={3}>
                        <Typography>Staring:</Typography>
                      </Grid>
                      <Grid item xs={9}>
                        <Typography className={classes.label}>{getCastsName(movieCasts)}</Typography>
                      </Grid>
                    </Fragment>
                  )}
                  {directorName && (
                    <Fragment>
                      <Grid item xs={3}>
                        <Typography>Directed by:</Typography>
                      </Grid>
                      <Grid item xs={9}>
                        <Typography className={classes.label}>{directorName}</Typography>
                      </Grid>
                    </Fragment>
                  )}
                </Grid>
              )}
            </Box>
          </Grid>
        </Grid>
      ) : (
        <Box className={classes.loaderWrapper}>
          <CircularProgress />
        </Box>
      )}
    </Container>
  );
});
