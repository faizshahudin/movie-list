import React, {  useEffect, Fragment, useRef} from "react";
import { useStateValue } from "../store";
import { getMovies } from "../actions/movie";
import MovieCategory from "../components/MovieCategory";
import { getScrollDownPercentage } from "../utils";
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles(theme => ({
  loaderWrapper: {
    textAlign: "center",
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(4)
  },
}))

export default () => {
  const classes = useStyles();
  const [{ movie }, dispatch] = useStateValue();
  const loaderRef = useRef(null);

  const handleScroll = () => {
    if (!movie.moviesFetch) {
      let percentageScrolled = getScrollDownPercentage(window);
      if (percentageScrolled > 0.8) {
        const nextPage = movie.page + 1;
        !movie.reachedLastPage && dispatch({ type: 'page-assign', payload: nextPage })
      }
    } else {
      // show loader when refetch
      window.scrollTo(0, loaderRef.current.offsetTop)
    }
  };

  useEffect(() => {
    getMovies({ page: movie.page, dispatch });
  }, [dispatch, movie.page]);

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  });
    
    return (
      <Fragment>
        {movie.movies.map(item => (
          <MovieCategory title={item.row_name} list={item.data} />
         ))}
         {movie.moviesFetch && (
           <Box className={classes.loaderWrapper} ref={loaderRef}>
            <CircularProgress />
          </Box>
         )}
      </Fragment>
    )
};
