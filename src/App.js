import React from "react";
import { BrowserRouter as Switch, Route } from "react-router-dom";
import { StoreProvider, initialState } from "./store";
import { mainReducer } from "./reducers";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import Slide from "@material-ui/core/Slide";
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import MovieList from "./pages/MovieList";
import MovieDetail from "./pages/MovieDetail";

const useStyles = makeStyles({
  root: {
    flexGrow: 1
  },
  content: {
    marginTop: 70
  }
});

const HideOnScroll = props => {
  const { children, window } = props;

  const trigger = useScrollTrigger({ target: window ? window(): undefined })

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  )
}

export default props => {
  const classes = useStyles();

  return (
    <StoreProvider initialState={initialState} reducer={mainReducer}>
      <div className={classes.root}>
        <HideOnScroll {...props}>
          <AppBar color="primary">
            <Container>
              <Toolbar disableGutters={true}>
                <Typography variant="h6" color="inherit">
                  Movies
                </Typography>
              </Toolbar>
            </Container>
          </AppBar>
        </HideOnScroll>
        <Box className={classes.content}>
          <Switch>
            <Route exact path="/" component={MovieList} />
            <Route exact path="/detail/:movieId" component={MovieDetail} />
          </Switch>
        </Box>
      </div>
    </StoreProvider>
  );
}
