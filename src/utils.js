export const getScrollDownPercentage = window => {
  const pageHeight = window.document.documentElement.scrollHeight;
  const clientHeight = window.document.documentElement.clientHeight;
  const scrollPos = window.pageYOffset;
  const currentPosition = scrollPos + clientHeight;
  const percentageScrolled = currentPosition / pageHeight;
  return percentageScrolled;
};

export const displayDuration = duration =>
  duration ? duration.replace(/h/g, " hours").replace(/m/g, " minutes") : ''

export const getCastsName = movieCasts => 
  movieCasts.map(item => item.name).toString().replace(/,/g, ', ')
